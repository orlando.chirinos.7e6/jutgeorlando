
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.util.*

val scan = Scanner(System.`in`)
//======================================================================================================================
val userfiles = File("src/main/resources/userdata.json")
var publicfiles = File("./src/main/resources/publicproblems.json")
var privatefiles = File("./src/main/resources/privateproblems.json")
var historydata = File("src/main/resources/history.json")

var problemChoice = ""

var user: User_log? = null

var matchdata = History(
    husername = "",
    hprivatepublic = "",
    hproblemid = 0,
    hproblemname = "",
    hproblemtype = "",
    hproblemtries = 0,
    hproblemsolved = false
)

//======================================== [CLASES] ========================================
@Serializable
data class User_log(
    val userid: Int,
    val username: String,
    val password: String,
    val usertype: Char
)

@Serializable
data class History(
    var husername: String,
    var hprivatepublic: String,
    var hproblemid: Int,
    var hproblemname: String,
    var hproblemtype: String,
    var hproblemtries: Int,
    var hproblemsolved: Boolean
)

@Serializable
data class PublicProblems(
    val name: String,
    val type: String,
    val description: String,
    val input: String,
    val output: String,
    val solution: String,
    var solved: Boolean,
    var tries: Int,
    var problemid: Int
)

@Serializable
data class PrivateProblems(
    val name: String,
    val type: String,
    val description: String,
    val input: String,
    val output: String,
    val solution: String,
    var solved: Boolean,
    var tries: Int,
    var problemid: Int
)
//======================================== [MENU] ========================================
fun usermenu() {
    //=============================================================================
    var loginchoice = ""
//    var registerchoice = ""
//    var useriddata = 0
//    var usertypedata = ' '
    //=============================================================================
    var welcometext =
        "${Kolor.foreground("                              [B I E N V E N I D O]", Color.LIGHT_GREEN)}\n\n" +
                "${
                    Kolor.foreground(
                        "Bienvenido al programa, querido usuario. Para poder acceder es necesario que inicie sesión en el sistema.",
                        Color.LIGHT_GREEN
                    )
                }\n" +
                "${Kolor.foreground("[R] Registrarse como un nuevo usuario al sistema.", Color.LIGHT_GREEN)}\n" +
                "${
                    Kolor.foreground(
                        "[I] Iniciar sesión con su cuenta actual de usuario en el sistema.",
                        Color.LIGHT_GREEN
                    )
                }\n" +
                Kolor.foreground(" > ", Color.GREEN)

    //=============================================================================
    println(Kolor.foreground("============================================================================================================", Color.WHITE))

    while (loginchoice != "r" && loginchoice != "i") {
        print(welcometext)
        loginchoice = scan.next().toLowerCase()

        if (loginchoice != "r" && loginchoice != "i") {
            println(Kolor.foreground("============================================================================================================", Color.WHITE))
            welcometext = "${Kolor.foreground("                                   [E R R O R]", Color.LIGHT_RED)}\n\n" +
                    "${Kolor.foreground("Para iniciar sesión en el sistema es necesario introducir una opción válida, intente de nuevo.", Color.LIGHT_YELLOW)}\n" +
                    "${Kolor.foreground("[R] Registrarse como un nuevo usuario al sistema.", Color.LIGHT_GREEN)}\n" +
                    "${Kolor.foreground("[I] Iniciar sesión con su cuenta actual de usuario en el sistema.", Color.LIGHT_GREEN)}\n" +
                    Kolor.foreground(" > ", Color.GREEN)
        }
    }
    //REGISTRAR NUEVO USUARIO
    if (loginchoice == "r") { register() }

    //INICIAR SESIÓN CON USUARIO YA EXISTENTE
    else {
        val userinlist = login()
        if (userinlist == null){ usermenu() }
        else{ menu(user) }
    }

}
//======================================== [REGISTRAR USUARIO] ========================================
fun register(){

    // DECLARACIÓN DE VARIABLES PRE-USO

    //========================
    var existentuser = false
//    var user: User_log? = null
    var usernamedata = ""
    var userpassworddata = ""
    var registerchoice = ""
    var useriddata = 0
    var usertypedata = ' '
    //=========================
    var passwordinfotext = (Kolor.foreground("Introduzca su contraseña, aviso: ${Kolor.foreground("la contraseña ha de ser de 5 caracteres: ", Color.LIGHT_YELLOW)}", Color.LIGHT_GREEN))

    var usertypetext = (Kolor.foreground(
        "Introduzca su tipo de usuario:\n" +
                "[A] = Alumno\n" +
                "[P] = Profesor\n" +
                " > ", Color.LIGHT_GREEN))
    // ==================================
    // PEDIR USERNAME
    print(Kolor.foreground("Introduzca nombre de usuario: ", Color.LIGHT_GREEN))
    usernamedata = scan.next().toUpperCase()
    // ==================================

    // PEDIR CONTRASEÑA, HASTA QUE EL USUARIO NO CUMPLA EL REQUISITO NO AVANZA

    while (userpassworddata.length != 5) {
        print(passwordinfotext)
        userpassworddata = scan.next()

        if (userpassworddata.length < 5) {
            passwordinfotext = (Kolor.foreground("Su contraseña no es lo suficientemente larga. ${Kolor.foreground("La contraseña ha de ser de 5 caracteres: ", Color.LIGHT_RED)}", Color.LIGHT_YELLOW)) }

        else { passwordinfotext = (Kolor.foreground("Su contraseña excede el límite de caracteres. ${Kolor.foreground("La contraseña ha de ser de 5 caracteres: ", Color.LIGHT_RED)}", Color.LIGHT_YELLOW))}
    }
    // ==================================

    // PEDIR TIPO DE USUARIO, HASTA QUE NO SEA VÁLIDO NO SE PROGRESA.
    while (usertypedata != 'p' && usertypedata != 'a') {
        print(usertypetext)
        usertypedata = scan.next().single().toLowerCase()

        if (usertypedata != 'p' && usertypedata != 'a') {
            usertypetext = "\n${
                Kolor.foreground("Instrucción introducida no admisible. Escoja una opción válida:", Color.LIGHT_YELLOW)}\n" +
                    Kolor.foreground(
                        "[A] = Alumno\n" +
                                "[P] = Profesor\n" +
                                " > ", Color.LIGHT_GREEN) }
    }
    // ==================================
    // ACTUALIZA EL USER ID
    val userlist = mutableListOf<User_log>()
    for (i in userfiles.readLines().iterator()) {
        userlist.add(Json.decodeFromString(i)) }

    userlist.forEach {
        while (it.userid == useriddata)
            useriddata++ }
    // ==================================

    // PREGUNTA AL USUARIO SI DESEA INTRODUCIR ESOS DATOS AL SISTEMA.

    while (registerchoice != "s" && registerchoice != "n") {
        print(Kolor.foreground("¿Está seguro de querer introducir estos datos?: [S] / [N]: ", Color.LIGHT_GREEN))
        registerchoice = scan.next().toLowerCase()

        if (registerchoice != "s" && registerchoice != "n") {
            println(Kolor.foreground("Instrucción introducida no admisible. Escoja una opción válida.", Color.LIGHT_YELLOW)) }
    }
    // ==================================

    // EN CASO DE INTRODUCIR LOS DATOS, SE APLICA EN EL JSON, CASO CONTRARIO, REGRESA AL SISTEMA.

    if (registerchoice == "s") {

        // ==================================

        //VERIFICAR SI EL USUARIO YA EXISTE

        for (i in userlist) {
            if (usernamedata == i.username) {
                existentuser = true
                println(Kolor.foreground("[USUARIO YA REGISTRADO]",Color.LIGHT_RED))
                useriddata = 0
                usermenu()
            }
        }

        // ==================================

        if (!existentuser) {
            val userdata = User_log(
                userid = useriddata,
                username = usernamedata,
                password = userpassworddata,
                usertype = usertypedata )

            val newuser = Json.encodeToString(userdata)
            userfiles.appendText("\n$newuser")
            usermenu() }
    }
    else { usermenu() }
}
//======================================== [LOGUEARSE CON EL USUARIO] ========================================
fun login():User_log? {

    //DECLARACIÓN DE VARIABLES PRE-USO
    val userlist = mutableListOf<User_log>()
    var usernamedata = ""
    var userpassworddata = ""
    var passwordtries = 3


    print(Kolor.foreground("Indique su nombre de usuario: ", Color.LIGHT_GREEN))
    usernamedata = scan.next().toUpperCase()

    //================================
    for (i in userfiles.readLines().iterator()) {
        userlist.add(Json.decodeFromString(i)) }

    userlist.forEach {
        if (it.username == usernamedata) {
            user = it }
    }
    //================================

    if (user == null) {
        println(Kolor.foreground("El usuario indicado no existe, volviendo al menú de inicio . . .", Color.LIGHT_RED)) }
    else {
        while (passwordtries > 0) {

            print(Kolor.foreground("Introduzca su contraseña de 5 caracteres: ", Color.LIGHT_GREEN))
            userpassworddata = scan.next()

            if (user!!.password != userpassworddata) {
                println(Kolor.foreground("[CONTRASEÑA INCORRECTA, LE QUEDAN ${passwordtries-1} INTENTOS]", Color.LIGHT_RED))
                passwordtries-- }

            else {
                if (user!!.password == userpassworddata && user!!.usertype == 'a') {
                    println(Kolor.foreground("Bienvenido al sistema, alumno [${user!!.username}]", Color.LIGHT_CYAN))
                return  user}

                else if (user!!.password == userpassworddata && user!!.usertype == 'p') {
                    println(Kolor.foreground("Bienvenido al sistema, profesor [${user!!.username}]", Color.LIGHT_BLUE))
                return user}
            }
        }
    }
    return null
}
//======================================== [MENU DEL JUTGEITB] ========================================
fun menu(user: User_log?) {

    var menuChoice = ""

    while (true) {

        println(Kolor.foreground("=============================================================", Color.WHITE))
        print(
            "${Kolor.foreground("             ¡Bienvenido/a a JutgeITB!", Color.BLUE)}\n\n" +
                    "Seleccione a continuación la sección a la que desea acceder:\n" +
                    "${Kolor.foreground("[A] Seguir con el itinerario de aprendizaje.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[B] Lista de problemas.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[C] Consultar histórico de problemas resueltos.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[H] Ayuda.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[R] Modo creador.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[E] Salir.", Color.CYAN)}\n" +
                    Kolor.foreground("> ", Color.GREEN)
        )
        menuChoice = scan.next().toLowerCase()

        when (menuChoice) {
            "a" -> {println("[ITINERARIO]")
                print(Kolor.foreground("PULSE [ENTER] PARA CONTINUAR", Color.LIGHT_GREEN))
                var intro = readln() }
            "b" -> {enunciados() }
            "c" -> {historic(user)
                    print(Kolor.foreground("PULSE [ENTER] PARA CONTINUAR", Color.LIGHT_GREEN))
                    var intro = readln()}
            "h" -> {
                print(
                    "${Kolor.foreground("==================================================================================================", Color.WHITE)}\n" +
                            "${Kolor.foreground(
                                      "Éste programa contiene distintos problemas de programación los cuales podrá resolver y entregar.\n" +
                                            "La interfaz es lo más intuitiva posible, por lo que no debería tener problemas durante su estancia.\n" +
                                            "Hay que recordar que éste programa sólo tiene disponibles 4 tipos de problemas [Tipo de datos, Condicionales, Bucles, Listas]\n"+
                                            "En caso de tener algún inconveniente contactar con: ", Color.WHITE)} " +
                            "${Kolor.foreground("orlando.chirinos.7e6@itb.cat", Color.LIGHT_YELLOW)}\n" +
                            "${Kolor.foreground("==================================================================================================\n", Color.WHITE)}\n" +
                            Kolor.foreground("PULSE [ENTER] PARA CONTINUAR", Color.LIGHT_GREEN)
                     )
                var intro = readln()
                    }
            "r" -> {
                if (user != null) {
                    if (user.usertype != 'p'){
                        println(Kolor.foreground("[MODO SÓLO DISPONIBLE PARA PROFESORES]",Color.LIGHT_YELLOW))
                        print(Kolor.foreground("PULSE [ENTER] PARA CONTINUAR", Color.LIGHT_GREEN))
                        var intro = readln()}
                    else { createproblem() }
                        }
                    }
            "e" -> {
                var exit = ""
                while (exit != "s" && exit != "n"){
                    print(Kolor.foreground("¿Está seguro de querer salir del sistema? [S] [N]: ", Color.LIGHT_YELLOW))
                    exit = scan.next().toLowerCase()

                    if (exit != "s" && exit != "n"){println(Kolor.foreground("\nInstrucción introducida no admisible, intente nuevamente.", Color.LIGHT_YELLOW))}
                }
                if (exit == "s"){break}
                else{println("[ERROR]")}
            }
            else -> println(Kolor.foreground("\nInstrucción introducida no admisible, intente nuevamente.", Color.LIGHT_YELLOW))
        }
    }
}
//======================================== [ENUNCIADOS] ========================================
fun enunciados() {

    problemChoice = ""

    while (problemChoice != "p" && problemChoice != "v") {

        println(Kolor.foreground("=============================================================", Color.WHITE))
        println(   "\nIntroduzca el tipo de pruebas que desee probar:\n\n" +
                    "${Kolor.foreground("[P] Juego de pruebas público.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[V] Juego de pruebas privado.", Color.CYAN)}\n" )

        problemChoice = scan.next().toLowerCase()

        when (problemChoice) {
            "p"  -> { user?.let { public(it) } }
            "v"  -> user?.let { private(it) }
            else -> println(Kolor.foreground("\nInstrucción introducida no admisible, intente nuevamente.",Color.LIGHT_YELLOW))
        }
    }
}
//======================================== [PROBLEMAS PÚBLICOS] ========================================
fun public(user: User_log){

    var skip_solve = ""
    var choice = ""
    val publicfiles2 = mutableListOf<PublicProblems>()

    for (i in publicfiles.readLines().iterator()) {
        publicfiles2.add(Json.decodeFromString(i))
    }

    var defaulttext = "\nCategoría de problemas:\n" +
            "${Kolor.foreground("[T] = Tipos de dato.", Color.CYAN)}\n" +
            "${Kolor.foreground("[C] = Condicionales.", Color.CYAN)}\n" +
            "${Kolor.foreground("[B] = Bucles.", Color.CYAN)}\n" +
            "${Kolor.foreground("[L] = Listas.", Color.CYAN)}\n" +
            Kolor.foreground("Indique su elección: ", Color.GREEN)

    while (choice != "t" && choice != "c" && choice != "b" && choice != "l") {
        print(defaulttext)
        choice = scan.next().toLowerCase()

        if (choice != "t" && choice != "c" && choice != "b" && choice != "l") {
            defaulttext = "${Kolor.foreground("\nOpción introducida no válida. ", Color.LIGHT_RED)}\n" +
                    "${Kolor.foreground("[T] = Tipos de datos.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[C] = Condicionales.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[B] = Bucles.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[L] = Listas.", Color.CYAN)}\n" +
                    Kolor.foreground("Indique su elección: ", Color.GREEN)
        }
    }

    when (choice) {
        "t" -> choice = "tipo de datos"
        "c" -> choice = "condicionales"
        "b" -> choice = "bucles"
        "l" -> choice = "listas"
    }

    publicfiles2.forEach() {

        if (it.type.toLowerCase() == choice) {
            skip_solve = ""

            val problem_compare = PublicProblems(
                name = it.name,
                type = it.type.toLowerCase(),
                description = it.description,
                input = it.input,
                output = it.output,
                solution = it.solution,
                solved = it.solved,
                tries = it.tries,
                problemid = it.problemid,
            )

            print("${Kolor.foreground("=====================================================================================================================", Color.WHITE)}\n" +
                    "${Kolor.foreground(it.name, Color.BLUE)}\n" +
                    "${Kolor.foreground(it.type, Color.CYAN)}\n" +
                    "${Kolor.foreground("ENUNCIADO:", Color.WHITE)} ${Kolor.foreground(it.description, Color.WHITE)}\n" +
                    "${Kolor.foreground("INPUT:", Color.WHITE)} ${Kolor.foreground(it.input, Color.WHITE)}\n" +
                    "${Kolor.foreground("OUTPUT:", Color.WHITE)} ${Kolor.foreground(it.output, Color.WHITE)}\n" +
                    "${Kolor.foreground("=====================================================================================================================", Color.WHITE)}\n")

            while (skip_solve != "r" && skip_solve != "s") {
                print("${Kolor.foreground("Tome una decisión: ", Color.LIGHT_CYAN)}\n" +
                            "${Kolor.foreground("Resolver problema [R]", Color.GREEN)}\n" +
                            "${Kolor.foreground("Saltar el problema [S]", Color.GREEN)}\n" +
                               Kolor.foreground(" > ", Color.GREEN)
                )
                skip_solve = scan.next().toLowerCase()

                var surrender = ""

                if (skip_solve == "r"){
                   do {
                       print(Kolor.foreground("Introduzca la respuesta al problema: ", Color.LIGHT_BLUE))
                       val intsolution = scan.next()

                       if (intsolution == problem_compare.solution) {
                           print("${Kolor.foreground("[HAS ACERTADO]", Color.LIGHT_GREEN)}\n")
                           problem_compare.tries++
                           problem_compare.solved = true

                       } else {
                           print("${Kolor.foreground("[HAS FALLADO]", Color.LIGHT_RED)}\n")
                           problem_compare.tries++
                           print(Kolor.foreground("¿DESEA RENDIRSE? [S / ANY KEY]: ", Color.LIGHT_YELLOW))
                           surrender = readln().toLowerCase()

                           if (surrender == "s"){break}
                       }
                   }while (intsolution != problem_compare.solution || surrender == "n")

                    print(Kolor.foreground(" Presione [ENTER] para continuar ", Color.LIGHT_GREEN))
                    val enter = readln()
                    historyadd(user, problem_compare)
                }
                else if (skip_solve == "s"){ println(Kolor.foreground("[SALTANDO PROBLEMA]", Color.LIGHT_GREEN)) }
                else {println(Kolor.foreground("[INSTRUCCIÓN NO ENCONTRADA, INTENTE NUEVAMENTE]",Color.LIGHT_YELLOW))}
            }
        }
    }
}
//======================================== [PRIVADOS] ========================================
fun private(user: User_log){

    var skip_solve = ""
    var choice = ""
    val privatefiles2 = mutableListOf<PrivateProblems>()

    for (i in privatefiles.readLines().iterator()) {
        privatefiles2.add(Json.decodeFromString(i))
    }

    var defaulttext = "\nCategoría de problemas:\n" +
            "${Kolor.foreground("[T] = Tipos de dato.", Color.CYAN)}\n" +
            "${Kolor.foreground("[C] = Condicionales.", Color.CYAN)}\n" +
            "${Kolor.foreground("[B] = Bucles.", Color.CYAN)}\n" +
            "${Kolor.foreground("[L] = Listas.", Color.CYAN)}\n" +
            Kolor.foreground("Indique su elección: ", Color.GREEN)

    while (choice != "t" && choice != "c" && choice != "b" && choice != "l") {
        print(defaulttext)
        choice = scan.next().toLowerCase()

        if (choice != "t" && choice != "c" && choice != "b" && choice != "l") {
            defaulttext = "${Kolor.foreground("\nOpción introducida no válida. ", Color.LIGHT_RED)}\n" +
                    "${Kolor.foreground("[T] = Tipos de datos.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[C] = Condicionales.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[B] = Bucles.", Color.CYAN)}\n" +
                    "${Kolor.foreground("[L] = Listas.", Color.CYAN)}\n" +
                    Kolor.foreground("Indique su elección: ", Color.GREEN)
        }
    }
    when (choice) {
        "t" -> choice = "tipo de datos"
        "c" -> choice = "condicionales"
        "b" -> choice = "bucles"
        "l" -> choice = "listas"
    }
    privatefiles2.forEach() {
        if (it.type.toLowerCase() == choice) {
            skip_solve = ""

            val problem_compare = PublicProblems(
                name = it.name,
                type = it.type,
                description = it.description,
                input = it.input,
                output = it.output,
                solution = it.solution,
                solved = it.solved,
                tries = it.tries,
                problemid = it.problemid,
            )
            print(  "${Kolor.foreground("=====================================================================================================================", Color.WHITE)}\n" +
                    "${Kolor.foreground(it.name, Color.BLUE)}\n" +
                    "${Kolor.foreground(it.type, Color.CYAN)}\n" +
                    "${Kolor.foreground("ENUNCIADO:", Color.WHITE)} ${Kolor.foreground(it.description, Color.WHITE)}\n" +
                    "${Kolor.foreground("INPUT:", Color.WHITE)} ${Kolor.foreground(it.input, Color.WHITE)}\n" +
                    "${Kolor.foreground("OUTPUT:", Color.WHITE)} ${Kolor.foreground(it.output, Color.WHITE)}\n" +
                    "${Kolor.foreground("Solución:", Color.WHITE)} ${Kolor.foreground(it.solution, Color.GREEN)}\n" +
                    "${Kolor.foreground("¿Resuelto?:", Color.WHITE)} ${Kolor.foreground(it.solved.toString(), Color.LIGHT_YELLOW)}\n" +
                    "${Kolor.foreground("Intentos:", Color.WHITE)} ${Kolor.foreground(it.tries.toString(), Color.LIGHT_YELLOW)}\n" +
                    "${Kolor.foreground("=====================================================================================================================", Color.WHITE)}\n")

            while (skip_solve != "r" && skip_solve != "s") {
                print("${Kolor.foreground("Tome una decisión: ", Color.LIGHT_CYAN)}\n" +
                        "${Kolor.foreground("Resolver problema [R]", Color.GREEN)}\n" +
                        "${Kolor.foreground("Saltar el problema [S]", Color.GREEN)}\n" +
                        Kolor.foreground(" > ", Color.GREEN))
                skip_solve = scan.next().toLowerCase()

                var surrender = ""
                if (skip_solve == "r"){
                    do {
                        print(Kolor.foreground("Introduzca la respuesta al problema: ", Color.LIGHT_BLUE))
                        val intsolution = scan.next()

                        if (intsolution == problem_compare.solution) {
                            print("${Kolor.foreground("[HAS ACERTADO]", Color.LIGHT_GREEN)}\n")
                            problem_compare.tries++
                            problem_compare.solved = true
                        } else {
                            print("${Kolor.foreground("[HAS FALLADO]", Color.LIGHT_RED)}\n")
                            problem_compare.tries++
                            print(Kolor.foreground("¿DESEA RENDIRSE? [S / ANY KEY]: ", Color.LIGHT_YELLOW))
                            surrender = readln().toLowerCase()

                            if(surrender == "s"){break}
                        }
                    }while (intsolution != problem_compare.solution || surrender == "n")

                    print(Kolor.foreground(" Presione [ENTER] para continuar ", Color.LIGHT_GREEN))
                    val enter = readln()
                    historyadd(user, problem_compare)
                }
                else if (skip_solve == "s"){ println(Kolor.foreground("[SALTANDO PROBLEMA]", Color.LIGHT_GREEN)) }
                else {println(Kolor.foreground("[INSTRUCCIÓN NO ENCONTRADA, INTENTE NUEVAMENTE]",Color.LIGHT_YELLOW))}
            }
        }
    }
}
//======================================== [HISTORIAL AÑADIR] ========================================
fun historyadd(user:User_log, problem_compare: PublicProblems) {

    val historydata2 = mutableListOf<History>()
    val historyToUpdate2 = mutableListOf<History>()

    val sameID = File("src/main/resources/historycopyto.json")
    val notID = File("src/main/resources/updatedhistory.json")


    matchdata = History(
        husername = user.username,
        hprivatepublic = problemChoice,
        hproblemid = problem_compare.problemid,
        hproblemname = problem_compare.name,
        hproblemtype = problem_compare.type,
        hproblemtries = problem_compare.tries,
        hproblemsolved = problem_compare.solved
    )

    //================================================================================
    for (i in historydata.readLines()) { historydata2.add(Json.decodeFromString(i)) }

    historydata2.forEach {
        val readHistory = History(
            husername = it.husername,
            hprivatepublic = problemChoice,
            hproblemid = it.hproblemid,
            hproblemname = it.hproblemname,
            hproblemtype = it.hproblemtype,
            hproblemtries = it.hproblemtries,
            hproblemsolved = it.hproblemsolved
        )

        if (readHistory.hproblemid == matchdata.hproblemid) {
            val addEdited = Json.encodeToString(matchdata)
            sameID.appendText("$addEdited\n")
        }
    }
    //================================================================================
    for (i in historydata.readLines()) { historyToUpdate2.add(Json.decodeFromString(i)) }

    historyToUpdate2.forEach {
        val readHistory = History(
            husername = it.husername,
            hprivatepublic = problemChoice,
            hproblemid = it.hproblemid,
            hproblemname = it.hproblemname,
            hproblemtype = it.hproblemtype,
            hproblemtries = it.hproblemtries,
            hproblemsolved = it.hproblemsolved
        )
        if (readHistory.hproblemid != matchdata.hproblemid) {
            val addNew = Json.encodeToString(readHistory)
            notID.appendText("$addNew\n")
        }
    }



}

fun updatedata() {

    val historyData2 = mutableListOf<History>()
    val copyToData = mutableListOf<History>()
    val copyData = File("src/main/resources/historycopyto.json")
    val updateFile = File("src/main/resources/updatedhistory.json")

    var historyToUpdate = History(
        husername = "",
        hprivatepublic = "",
        hproblemid = 0,
        hproblemname = "",
        hproblemtype = "",
        hproblemtries = 0,
        hproblemsolved = false
    )

    var historyFiles = History(
        husername = "",
        hprivatepublic = "",
        hproblemid = 0,
        hproblemname = "",
        hproblemtype = "",
        hproblemtries = 0,
        hproblemsolved = false
    )

    //HistoryData2
    for (i in historydata.readLines()) { historyData2.add(Json.decodeFromString(i)) }
    //Copytodata
    for (i in copyData.readLines()) { copyToData.add(Json.decodeFromString(i)) }

    copyToData.forEach {

        historyToUpdate = History(
            husername = it.husername,
            hprivatepublic = problemChoice,
            hproblemid = it.hproblemid,
            hproblemname = it.hproblemname,
            hproblemtype = it.hproblemtype,
            hproblemtries = it.hproblemtries,
            hproblemsolved = it.hproblemsolved
        )


        val sameID = Json.encodeToString(historyToUpdate)
        updateFile.appendText("$sameID\n")



    }


/*    historydata.delete()
    updateFile.copyTo(historydata)*/

}
//======================================== [||||||||] ========================================
fun historic(user: User_log?) {

    val historydata2 = mutableListOf<History>()

    for (i in historydata.readLines().iterator()) {
        historydata2.add(Json.decodeFromString(i))
    }

    historydata2.forEach {
        if (user != null) {
            if (it.husername == user.username) {
                print(  "${Kolor.foreground("========================================================", Color.WHITE)}\n" +
                        "${Kolor.foreground("Usuario: ", Color.WHITE)} " +
                        "${Kolor.foreground(it.husername, Color.LIGHT_CYAN)}\n")
                //==========================================
                if (it.hprivatepublic == "p") {
                    print("${Kolor.foreground("Privacidad de problema: ", Color.WHITE)} ${Kolor.foreground("PÚBLICO", Color.CYAN)}\n") }
                else {
                    print("${Kolor.foreground("Privacidad de problema: ", Color.WHITE)} ${Kolor.foreground("PRIVADO", Color.BLUE )}\n") }
                //==========================================
                print(  "${Kolor.foreground("ID del problema: ", Color.WHITE)} ${Kolor.foreground("${it.hproblemid}", Color.LIGHT_MAGENTA)}\n" +
                        "${Kolor.foreground("Nombre del problema: ", Color.WHITE)} ${Kolor.foreground(it.hproblemname,Color.BLUE)}\n" +
                        "${Kolor.foreground("Tipo de problema: ", Color.WHITE)} ${Kolor.foreground(it.hproblemtype, Color.LIGHT_BLUE)}\n" +
                        "${Kolor.foreground("Intentos: ", Color.WHITE)} ${Kolor.foreground("${it.hproblemtries}", Color.LIGHT_GREEN)}\n")
                //==========================================
                if (!it.hproblemsolved) {
                    print("${Kolor.foreground("¿Solucionado?: ", Color.WHITE)} ${Kolor.foreground("${it.hproblemsolved}", Color.LIGHT_RED)}\n") }
                else {
                    print("${Kolor.foreground("¿Solucionado?: ", Color.WHITE)} ${Kolor.foreground("${it.hproblemsolved}",Color.LIGHT_GREEN)}\n") }
                //==========================================
                print("${Kolor.foreground("========================================================", Color.WHITE)}\n")
            }
        }
    }
}
//======================================== [||||||||] ========================================
fun createproblem(){

    var create_choice = ""

    while (true){
        print("${Kolor.foreground("            [BIENVENIDO AL MODO CREADOR]",Color.LIGHT_GREEN)}\n" +
                "${Kolor.foreground("En éste modo podrá crear nuevos problemas para sus estudiantes\nSeleccione el tipo de problema que desea crear: ",Color.LIGHT_GREEN)}\n" +
                "${Kolor.foreground("[P] Problema público.\n" +
                                          "[V] Problema privado.\n" +
                                          "[E] Salir del modo creador.",Color.LIGHT_GREEN)}\n" +
                Kolor.foreground(" > ",Color.GREEN) )

        create_choice = scan.next().toLowerCase()

        when (create_choice){
            "p" -> createpublic()
            "v" -> createprivate()
            "e" -> break
            else -> println()
        }

    }

}
//======================================== [||||||||] ========================================
fun createpublic(){

    //================================
    var createChoice = ""
    var problemName = ""
    var problemType = ""
    var problemDescription = ""
    var problemInput = ""
    var problemOutput = ""
    var problemSolution = ""
    var problemidData = 0
    //================================

    println(Kolor.foreground("==================================================================================================", Color.WHITE))
    println(Kolor.foreground("       [BIENVENIDO AL MODO CREADOR DE PROBLEMAS PÚBLICOS]\n", Color.LIGHT_BLUE))

    for (i in 1..6){
        when (i){
            1 -> {
                print(Kolor.foreground("Introduzca el [TÍTULO] del problema:",Color.BLUE))
                problemName = scan.next().replace(',', ' ')
                println()
            }
            2 -> {
                print(Kolor.foreground("Introduzca el [TIPO DE PROBLEMA] del problema:",Color.BLUE))
                problemType = scan.next().replace(',', ' ')
                println()
            }
            3 -> {
                print(Kolor.foreground("Introduzca la [DESCRIPCIÓN] del problema:",Color.BLUE))
                problemDescription = scan.next().replace(',', ' ')
                println()
            }
            4 ->{
                print(Kolor.foreground("Introduzca [INPUT] del problema:",Color.BLUE))
                problemInput = scan.next().replace(',', ' ')
                println()
            }
            5 -> {
                print(Kolor.foreground("Introduzca [OUTPUT] del problema:",Color.BLUE))
                problemOutput = scan.next().replace(',', ' ')
                println()
            }
            6 -> {
                print(Kolor.foreground("Introduzca [SOLUCIÓN] del problema:",Color.BLUE))
                problemSolution = scan.next().replace(',', ' ')
                println()
            }
        }
    }

    // ==================================
    // ACTUALIZA EL USER ID
    val problemList = mutableListOf<PublicProblems>()
    for (i in publicfiles.readLines().iterator()) {
        problemList.add(Json.decodeFromString(i)) }

    problemList.forEach {
        while (it.problemid == problemidData)
            problemidData++ }
    // ==================================


    while (createChoice != "s" && createChoice != "n") {
        print(Kolor.foreground("¿Está seguro de querer introducir estos datos?: [S] / [N]: ", Color.LIGHT_GREEN))
        createChoice = scan.next().toLowerCase()

        if (createChoice == "s"){
            val problem_compare = PublicProblems(
                name = problemName,
                type = problemType,
                description = problemDescription,
                input = problemInput,
                output = problemOutput,
                solution = problemSolution,
                solved = false,
                tries = 0,
                problemid = problemidData,
            )
            val newProblem = Json.encodeToString(problem_compare)
            publicfiles.appendText("\n$newProblem")
            println(Kolor.foreground("[PROBLEMA CREADO EXITOSAMENTE]\n" +
                    "PRESIONE [ENTER] PARA CONTINUAR ",Color.LIGHT_GREEN))
            val enter = readln()
            println(Kolor.foreground("==================================================================================================", Color.WHITE))
        }
        else{
            println(Kolor.foreground("[CANCELANDO . . .]",Color.LIGHT_YELLOW))
            println(Kolor.foreground("==================================================================================================", Color.WHITE))
        }

        if (createChoice != "s" && createChoice != "n") {
            println(Kolor.foreground("Instrucción introducida no admisible. Escoja una opción válida.", Color.LIGHT_YELLOW)) }
            println(Kolor.foreground("==================================================================================================", Color.WHITE))

    }
}
//======================================== [||||||||] ========================================
fun createprivate(){
    //================================
    var createChoice = ""
    var problemName = ""
    var problemType = ""
    var problemDescription = ""
    var problemInput = ""
    var problemOutput = ""
    var problemSolution = ""
    var problemidData = 0
    //================================

    println(Kolor.foreground("==================================================================================================", Color.WHITE))
    println(Kolor.foreground("       [BIENVENIDO AL MODO CREADOR DE PROBLEMAS PRIVADOS]\n", Color.LIGHT_CYAN))

    for (i in 1..6){
        when (i){
            1 -> {
                print(Kolor.foreground("Introduzca el [TÍTULO] del problema:",Color.BLUE))
                problemName = scan.next().replace(',', ' ')
                println()
            }
            2 -> {
                print(Kolor.foreground("Introduzca el [TIPO DE PROBLEMA] del problema:",Color.BLUE))
                problemType = scan.next().replace(',', ' ')
                println()
            }
            3 -> {
                print(Kolor.foreground("Introduzca la [DESCRIPCIÓN] del problema:",Color.BLUE))
                problemDescription = scan.next().replace(',', ' ')
                println()
            }
            4 ->{
                print(Kolor.foreground("Introduzca [INPUT] del problema:",Color.BLUE))
                problemInput = scan.next().replace(',', ' ')
                println()
            }
            5 -> {
                print(Kolor.foreground("Introduzca [OUTPUT] del problema:",Color.BLUE))
                problemOutput = scan.next().replace(',', ' ')
                println()
            }
            6 -> {
                print(Kolor.foreground("Introduzca [SOLUCIÓN] del problema:",Color.BLUE))
                problemSolution = scan.next().replace(',', ' ')
                println()
            }
        }
    }

    // ==================================
    // ACTUALIZA EL USER ID
    val problemList = mutableListOf<PublicProblems>()
    for (i in privatefiles.readLines().iterator()) {
        problemList.add(Json.decodeFromString(i)) }

    problemList.forEach {
        while (it.problemid == problemidData)
            problemidData++ }
    // ==================================


    while (createChoice != "s" && createChoice != "n") {
        print(Kolor.foreground("¿Está seguro de querer introducir estos datos?: [S] / [N]: ", Color.LIGHT_GREEN))
        createChoice = scan.next().toLowerCase()

        if (createChoice == "s"){
            val problem_compare = PrivateProblems(
                name = problemName,
                type = problemType,
                description = problemDescription,
                input = problemInput,
                output = problemOutput,
                solution = problemSolution,
                solved = false,
                tries = 0,
                problemid = problemidData,
            )
            val newProblem = Json.encodeToString(problem_compare)
            privatefiles.appendText("\n$newProblem")
            println(Kolor.foreground("[PROBLEMA CREADO EXITOSAMENTE]\n" +
                    "PRESIONE [ENTER] PARA CONTINUAR ",Color.LIGHT_GREEN))
            val enter = readln()
            println(Kolor.foreground("==================================================================================================", Color.WHITE))
        }
        else{
            println(Kolor.foreground("[CANCELANDO . . .]",Color.LIGHT_YELLOW))
            println(Kolor.foreground("==================================================================================================", Color.WHITE))
        }

        if (createChoice != "s" && createChoice != "n") {
            println(Kolor.foreground("Instrucción introducida no admisible. Escoja una opción válida.", Color.LIGHT_YELLOW)) }
        println(Kolor.foreground("==================================================================================================", Color.WHITE))
    }
}
//======================================== [||||||||] ========================================